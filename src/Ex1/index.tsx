import { Link } from "react-router-dom"
import Counter from "./Counter"

export default () => {

    return (<div>
        <h2>Exercise 1 - Counter</h2>
        <Link to='/'>Back</Link>

        <Counter title='Marbles' />
    </div>)
}