import React from "react";

interface Props {
    title: string;
    initialValue?: number;
}

const DEFAULT_INITIAL_VALUE = 0;

const Counter: React.FC<Props> = ({ title, initialValue = DEFAULT_INITIAL_VALUE }) => {

    const [count, setCount] = React.useState<number>(initialValue);

    const increment = React.useCallback(() => setCount(c => c + 1), []);
    const decrement = React.useCallback(() => setCount(c => c - 1), []);

    return (<div>
        <h2>{title}</h2>
        <p>Count: {count}</p>
        <button role='button' onClick={increment}>Increment</button>
        <button role='button' onClick={decrement}>Decrement</button>
    </div>)
}

export default Counter;