/**
 * You will probably want all of these ...
 */
// import { render, screen } from "@testing-library/react"
// import userEvent from '@testing-library/user-event'

// import Counter from "./Counter"

describe("Exercise 1 - Counter", () => {
    it("Displays correctly", () => {
        // When rendered with a title, does it contain the title and initial count?
    })

    it("Displays correctly with custom initial count", () => {
        // When rendered with a custom initial count, does it use it?
    })

    it('Increments Correctly', async () => {
        // When the user clicks on increment, does it behave correctly?
    })

    it('Decrements Correctly', async () => {
        // When the user clicks on decrement, does it behave correctly?
    })
})