import { Link } from 'react-router-dom'

function App() {
  return (
    <>
      <h1>Software Craft Guild</h1>
      <p role="note">Joe Sharp doesn't do styling, that's his style</p>

      <ol>
        <li><Link to={'/ex1'}>Exercise 1 - Simple Counter</Link></li>
        <li><Link to={'/ex2'}>Exercise 2 - Forms and Tables</Link></li>
        <li><Link to={'/ex3'}>Exercise 3 - Delayed Content</Link></li>
      </ol>
    </>
  );
}

export default App;
