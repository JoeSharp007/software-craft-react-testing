import { Link } from "react-router-dom"
import PersonManager from "./PersonManager"

export default () => {
    return <div>
        <h2>Exercise 2 - Person Form</h2>
        <Link to='/'>Back</Link>
        <p>This exercise has two test suites to write:</p>
        <ol>
            <li>The custom hook that is used to manage a list of people</li>
            <li>The form element which is used to add new people</li>
        </ol>

        <PersonManager />
    </div>
}