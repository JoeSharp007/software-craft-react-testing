import React from 'react';

import Person from "./Person";

interface IManagePeople {
    people: Person[];
    addPerson: (person: Person) => void;
    deletePerson: (id: string) => void;
}

/**
 * A custom hook for managing a list of people.
 * 
 * @param initialList The initial list of people
 * @returns The custom hook API
 */
function useManagePeople(initialList: Person[] = []): IManagePeople {
    const [people, setPeople] = React.useState<Person[]>(initialList);

    const addPerson = React.useCallback((person: Person) => setPeople(ps => [...ps, person]), []);
    const deletePerson = React.useCallback((id: string) => setPeople(ps => ps.filter(p => p.id !== id)), []);

    return {
        people,
        addPerson,
        deletePerson
    }

}

export default useManagePeople;