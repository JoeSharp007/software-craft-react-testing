import React from 'react';
import { v4 as uuid } from 'uuid';
import Person from './Person';


interface Props {
    onSubmit: (person: Person) => void;
}

const PersonForm: React.FC<Props> = ({ onSubmit }) => {
    const [name, setName] = React.useState<string>('');
    const [age, setAge] = React.useState<number>(0);

    const onNameChange: React.ChangeEventHandler<HTMLInputElement> = React.useCallback(({ target: { value } }) => {
        setName(value);
    }, [])

    const onAgeChange: React.ChangeEventHandler<HTMLInputElement> = React.useCallback(({ target: { value } }) => setAge(parseInt(value, 10)), []);

    const onSubmitForm: React.FormEventHandler = React.useCallback((e) => {
        e.preventDefault();

        onSubmit({
            id: uuid(),
            name, age
        })
    }, [name, age, onSubmit])

    return <form onSubmit={onSubmitForm}>
        <div>
            <label htmlFor='name'>Name</label>
            <input name='name' type='text' placeholder='enter name' value={name} onChange={onNameChange} />
        </div>
        <div>
            <label htmlFor='age'>Age</label>
            <input name='age' type='number' min={0} max={150} value={age} onChange={onAgeChange} />
        </div>
        <button role='submit'>Add</button>
    </form>
}

export default PersonForm;