import { SERVER_STATE } from "./nameServer"

describe("ShowNames", () => {
    it('shows the names after a small delay', () => {
        // Initialise list (doing this the filthy way due to 'not real server')
        SERVER_STATE.names = ['adam', 'briony', 'cindy', 'david']

        // Render component
        // Do the names appear after a time?
        // Does waiting state disappear?
    })

    it('shows error correctly', () => {
        // Initialise list (doing this the filthy way due to 'not real server')
        SERVER_STATE.names = ['adam', 'briony', 'cindy', 'david']
        SERVER_STATE.inError = true;

        // Render component
        // Does the error show correctly?
        // Does waiting state disappear?
    })

    it('clicking refresh after server names change correctly refreshes', () => {
        // Initialise list (doing this the filthy way due to 'not real server')
        SERVER_STATE.names = ['adam', 'briony', 'cindy', 'david']

        // Render component
        // Do the names appear after a time?

        // We modify list...
        SERVER_STATE.names = ['adam', 'briony', 'cindy', 'david', 'eggbert']

        // Click Refresh
        // Does the new 'eggbert' value appear after a time?
    })
})