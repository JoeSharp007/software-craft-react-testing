import { SERVER_STATE } from "./nameServer";

/**
 * To simulate fetching from API (without having to standup a server)
 * This 'API' generates and returns a list of names
 * 
 * @returns A promise of a random number
 */
export default function fetchNames(): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        setInterval(() => {
            if (SERVER_STATE.inError) {
                reject('Server Error')
            } else {
                resolve(SERVER_STATE.names);
            }
        }, 500);
    })
}