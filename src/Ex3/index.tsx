import { Link } from "react-router-dom"
import ModifyServer from "./ModifyServer"
import ShowNames from "./ShowNames"

export default () => {
    return <div>
        <h2>Exercise 3 - Dealing with Delay</h2>
        <Link to='/'>Back</Link>
        <p>This exercise requires us to cope with components that load content</p>

        <ShowNames />
        <ModifyServer />
    </div>
}