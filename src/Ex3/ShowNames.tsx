import React from 'react';
import fetchNames from './fetchNames';

const ShowNames: React.FC = () => {
    const [waiting, setWaiting] = React.useState<boolean>(true);
    const [names, setNames] = React.useState<string[]>([]);
    const [error, setError] = React.useState<string | undefined>();

    const onRefresh = React.useCallback(() => {
        setWaiting(true);
        setNames([]);

        fetchNames().then(names => {
            setNames(names);
            setError(undefined);
        }).catch(err => {
            setError(err);
        }).finally(() => {
            setWaiting(false);
        });
    }, [fetchNames])

    React.useEffect(() => onRefresh(), [onRefresh])


    return <div>
        <h3>List of Names from Server</h3>
        {waiting && <div>Waiting for response...</div>}
        {error && <div>Error from server: {error}</div>}

        <ul>
            {names.map((name, i) => <li key={i}>{name}</li>)}
        </ul>

        <button role='button' onClick={onRefresh}>Refresh</button>
    </div>
}

export default ShowNames;